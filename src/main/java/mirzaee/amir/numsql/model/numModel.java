package mirzaee.amir.numsql.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amir on 17/09/2017.
 */

public class numModel extends SQLiteOpenHelper {
    SQLiteDatabase db;
    SQLiteDatabase db1;
    String qurcreate = "" +
            "CREATE TABLE hesab_tab (" +
            "_id INTEGER AUTO INCREMENT PRIMARY KEY ," +
            "tarakonesh INTEGER," +
            "hesab INTEGER," +
            "date INTEGER," +
            "noe INTEGER" +
            ")";

    String qurcreate2 = "" +
            "CREATE TABLE nhesab_tab (" +
            "id INTEGER ," +
            "hesab TEXT" +
            ")";
    String qurcreatenoe = "" +
            "CREATE TABLE noe_tab (" +
            "id INTEGER ," +
            "noe TEXT, " +
            "stat INTEGER,"+
            "child INTEGER"+
            ")";
    String primhesab = "INSERT INTO nhesab_tab(id ,hesab)VALUES('1','حساب شخصی')";
    String primnoed = "INSERT INTO noe_tab(id,child ,stat,noe)VALUES('1','0','1','نامشخص')";
    String primnoeh = "INSERT INTO noe_tab(id,child ,stat,noe)VALUES('2','0','0','نامشخص')";

    public numModel(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(qurcreate);
        sqLiteDatabase.execSQL(qurcreate2);
        sqLiteDatabase.execSQL(qurcreatenoe);
        sqLiteDatabase.execSQL(primhesab);
        sqLiteDatabase.execSQL(primnoed);
        sqLiteDatabase.execSQL(primnoeh);




    }

    public void insertnum(Integer tarakonesh, String hh,Integer tdate, Integer noe) {
        String hesabid = "1";
        db = this.getWritableDatabase();
        String test = "SELECT id FROM nhesab_tab WHERE hesab ='" + hh + "'";
        Cursor cursor = db.rawQuery(test, null);
        while (cursor.moveToNext()) {
            hesabid = cursor.getString(0);
        }

        String insertcomand = "" +
                "INSERT INTO hesab_tab(tarakonesh,hesab ,date, noe )" +
                "VALUES('" + tarakonesh + "','" + hesabid + "','"+tdate+"','" + noe + "')";
        db.execSQL(insertcomand);
        db.close();

    }

    public Integer getNum(String hesa) {
        db = this.getReadableDatabase();
        String hesabid = "1";
        String test = "SELECT id FROM nhesab_tab WHERE hesab ='" + hesa + "'";
        Cursor cursor = db.rawQuery(test, null);
        while (cursor.moveToNext()) {
            hesabid = cursor.getString(0);
        }
        Integer result = 0;
        Integer majmodar = 0;
        Integer majmohaz = 0;
        String redqurydar = "SELECT tarakonesh FROM hesab_tab WHERE noe=1 AND hesab ='" + hesabid + "'";
        String redquryhaz = "SELECT tarakonesh FROM hesab_tab WHERE noe=0 AND hesab ='" + hesabid + "'";
        Cursor dar = db.rawQuery(redqurydar, null);
        Cursor haz = db.rawQuery(redquryhaz, null);
        while (dar.moveToNext()) {
            majmodar += Integer.parseInt(dar.getString(0));
        }
        while (haz.moveToNext()) {
            majmohaz += Integer.parseInt(haz.getString(0));
        }
        result = majmodar - majmohaz;
        db.close();
        return result;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    public String getDefhesab(String hh) {
        long recc = 0;
        String rec = null, dd = "";
        String gethesab = "SELECT id FROM nhesab_tab WHERE hesab ='" + hh + "'";
        int ss = 0;
        Cursor x = db.rawQuery(gethesab, null);
//        if (x != null)
//        {
//            x.moveToFirst();
//            recc=x.getLong(0);
//            rec=String.valueOf(recc);
//        }
//        return ;

        while (x.moveToNext()) {
            dd = x.getString(0);
//            break;
        }db.close();
        return dd;
    }


    public void inserthesab(String hesab) {
        db = this.getWritableDatabase();
        int y = 0;
        String test = "SELECT id FROM nhesab_tab";
        Cursor cursor = db.rawQuery(test, null);
        while (cursor.moveToNext()) {
            if (y < Integer.parseInt(cursor.getString(0))) {
                y = Integer.parseInt(cursor.getString(0));
            }
        }
        y += 1;
        String insertcomand = "" +
                "INSERT INTO nhesab_tab(id,hesab)" +
                "VALUES('" + y + "','" + hesab + "')";
        db.execSQL(insertcomand);
        db.close();

    }

//    public String getTest() {
//        db = this.getReadableDatabase();
//        int count = 0;
//        String gethesab = "SELECT hesab FROM nhesab_tab ", dd = "";
//        Cursor x = db.rawQuery(gethesab, null);
////        if(x != null && !x.isClosed()){
////            count = x.getCount();
////            x.close();
////        }
//        while (x.moveToNext()) {
//            dd += x.getString(0) + "\n";
//
//        }
//        return dd;
//    }

    public List<String> getTest2() {
        db = this.getReadableDatabase();
        int count = 0;
        String gethesab = "SELECT hesab FROM nhesab_tab ";
        List<String> dd = new ArrayList<>();
        Cursor x = db.rawQuery(gethesab, null);
        while (x.moveToNext()) {
            dd.add(x.getString(0));
        }
        db.close();
        return dd;
    }

    public int getidhesab(String hes) {
        int i = 1;
        db = this.getReadableDatabase();
        String qer = "SELECT id  FROM nhesab_tab WHERE hesab='" + hes + "' ";
        Cursor x = db.rawQuery(qer, null);
        i = Integer.parseInt(x.toString());
        db.close();
        return i;
    }


    public int ruz(int no,int day,String hesab) {
        db = this.getReadableDatabase();
        int sum = 0;
        String hesabid = "1";
        String test = "SELECT id FROM nhesab_tab WHERE hesab ='" + hesab + "'";
        Cursor cursor1 = db.rawQuery(test, null);
        while (cursor1.moveToNext()) {
            hesabid = cursor1.getString(0);
        }
        String qury = "SELECT tarakonesh FROM hesab_tab WHERE noe='"+no+"' AND date ='"+day+"' AND hesab='"+hesabid+"' ";
        Cursor cursor = db.rawQuery(qury, null);
        while (cursor.moveToNext())
            sum = sum + Integer.parseInt(cursor.getString(0));
        db.close();
        return sum;
    }
    public int hafte(int no,int sweek,int eweek,String hesab) {
        db = this.getReadableDatabase();
        int sum = 0;
        String hesabid = "1";
        String test = "SELECT id FROM nhesab_tab WHERE hesab ='" + hesab + "'";
        Cursor cursor1 = db.rawQuery(test, null);
        while (cursor1.moveToNext()) {
            hesabid = cursor1.getString(0);
        }
        String qury = "SELECT tarakonesh FROM hesab_tab WHERE noe='"+no+"' AND date >='"+sweek+"' AND date <='"+eweek+"' AND hesab='"+hesabid+"' ";
        Cursor cursor = db.rawQuery(qury, null);
        while (cursor.moveToNext())
            sum = sum + Integer.parseInt(cursor.getString(0));
        db.close();
        return sum;

    }
    public int mah(int no,int smonth,int emonth,String hesab) {
        db = this.getReadableDatabase();
        int sum = 0;
        String hesabid = "1";
        String test = "SELECT id FROM nhesab_tab WHERE hesab ='" + hesab + "'";
        Cursor cursor1 = db.rawQuery(test, null);
        while (cursor1.moveToNext()) {
            hesabid = cursor1.getString(0);
        }
        String qury = "SELECT tarakonesh FROM hesab_tab WHERE noe='"+no+"' AND date >='"+smonth+"' AND date <'"+emonth+"' AND hesab='"+hesabid+"'  ";
        Cursor cursor = db.rawQuery(qury, null);
        while (cursor.moveToNext())
            sum = sum + Integer.parseInt(cursor.getString(0));
        db.close();
        return sum;
    }
    public int sal(int no,int ssal,int esal,String hesab) {
        db = this.getReadableDatabase();
        int sum = 0;
        String hesabid = "1";
        String test = "SELECT id FROM nhesab_tab WHERE hesab ='" + hesab + "'";
        Cursor cursor1 = db.rawQuery(test, null);
        while (cursor1.moveToNext()) {
            hesabid = cursor1.getString(0);
        }
        String qury = "SELECT tarakonesh FROM hesab_tab WHERE noe='"+no+"' AND date >='"+ssal+"' AND date <'"+esal+"'  AND hesab='"+hesabid+"'";
        Cursor cursor = db.rawQuery(qury, null);
        while (cursor.moveToNext())
            sum = sum + Integer.parseInt(cursor.getString(0));
        db.close();
        return sum;
    }

}
