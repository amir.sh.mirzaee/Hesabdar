package mirzaee.amir.numsql;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import mirzaee.amir.numsql.adapters.HesabAdapter;
import mirzaee.amir.numsql.model.numModel;

public class HesabActivity extends AppCompatActivity {
    numModel db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hesab);

        ListView hlist;
        hlist= (ListView)findViewById(R.id.hlist);
        db =new numModel(this,"hesab.db",null,1);

        HesabAdapter adapter = new HesabAdapter(this,db.getTest2());
        hlist.setAdapter(adapter);

        hlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Class contxclass = null;
                Intent intent1=getIntent();
                String contx=intent1.getStringExtra("contx");
                try {
                    contxclass = Class.forName(contx);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                Intent intent= new Intent(HesabActivity.this,contxclass);
                intent.putExtra("hid",db.getTest2().get(i));
            }
        });



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(HesabActivity.this , AddHesabActivity.class);
        startActivity(intent);
//        switch (item.getItemId()) {
//            case R.id.addbar:
//                // User chose the "Settings" item, show the app settings UI...
//                return true;
//
//            default:
//                // If we got here, the user's action was not recognized.
//                // Invoke the superclass to handle it.
        return super.onOptionsItemSelected(item);
//
//        }
    }
    @Override
    protected void onResume() {

        super.onResume();
        ListView hlist;
        hlist= (ListView)findViewById(R.id.hlist);
        HesabAdapter adapter = new HesabAdapter(this,db.getTest2());
        hlist.setAdapter(adapter);
    }
}
