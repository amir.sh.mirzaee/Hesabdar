package mirzaee.amir.numsql;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import mirzaee.amir.numsql.model.numModel;

public class DaramadActivity extends AppCompatActivity {
    EditText incame;
    Button insert;
    Integer noe = 1;
    numModel db;
    TextView ihesab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daramad);
        Intent intent=getIntent();
        String hid =intent.getStringExtra("hid");
        incame=(EditText)findViewById(R.id.income);
        insert=(Button)findViewById(R.id.insert);
        ihesab=(TextView)findViewById(R.id.ihesab);
        ihesab.setText (hid);

        db =new numModel(this,"hesab.db",null,1);

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.insertnum(Integer.parseInt(incame.getText().toString()),noe);
//                Intent intent = new Intent(DaramadActivity.this,MainActivity.class);
//                startActivity(intent);
                finish();

            }
        });
        ihesab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DaramadActivity.this,HesabActivity.class);
                intent.putExtra("contx",this.toString());
                startActivity(intent);
            }
        });

    }
}
