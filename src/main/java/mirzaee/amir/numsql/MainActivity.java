package mirzaee.amir.numsql;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mirzaee.amir.numsql.model.numModel;

public class MainActivity extends AppCompatActivity {
//    EditText daramad , hazine;
    TextView sum ,hesab,ldaramad ;
    Button daramad , hazine , newb;
    EditText newh;
//    Integer noe=1;
    numModel db;
    CheckBox chek;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        preparation();

        daramad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,DaramadActivity.class);
                intent.putExtra("hid",hesab.getText().toString());
                startActivity(intent);
            }
        });
        hazine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,HazinehActivity.class);
                startActivity(intent);
            }
        });
        hesab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this , HesabActivity.class);
                startActivity(intent);
            }
        });



        newb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.inserthesab(newh.getText().toString());
                ldaramad.setText(db.getTest());
            }
        });



//        db =new numModel(this,"hesab.db",null,1);

//        save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(chek.isChecked())
//                    noe=0;
//                else
//                    noe=1;
////                db.insertnum(Integer.parseInt(daramad.getText().toString()),noe);
//
////                natije.setText(db.getNum().toString());
//            }
//        });
    }

    private void bind() {
        daramad=(Button) findViewById(R.id.daramad);
        hazine=(Button) findViewById(R.id.hazineh);
        newb=(Button) findViewById(R.id.newb);
        sum=(TextView)findViewById(R.id.sum);
        hesab=(TextView)findViewById(R.id.hesab);
        ldaramad=(TextView)findViewById(R.id.ldaramad);
        newh=(EditText) findViewById(R.id.newh);

    }
    @Override
    protected void onResume() {

        super.onResume();
        sum.setText(db.getNum("").toString());
    }
    public void preparation(){
        bind();

        db =new numModel(this,"hesab.db",null,1);
        sum.setText(db.getNum("").toString());
        List <String> hes = new ArrayList<>();
        hes=db.getTest2();
        hesab.setText(hes.get(0));


    }
    public void openpage (){
        Intent intent = new Intent(MainActivity.this , HazinehActivity.class);
        startActivity(intent);
    }

}
