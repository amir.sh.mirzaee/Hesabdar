package mirzaee.amir.numsql.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import mirzaee.amir.numsql.R;

/**
 * Created by amir on 06/10/2017.
 */

public class HesabAdapter extends BaseAdapter {
    Context mcontext;
    List <String> hesabs;

    public HesabAdapter(Context mcontext, List<String> hesabs) {
        this.mcontext = mcontext;
        this.hesabs = hesabs;
    }

    @Override
    public int getCount() {
        return hesabs.size();

    }

    @Override
    public Object getItem(int position) {
        return hesabs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mcontext).inflate(R.layout.hesab_list_item, viewGroup, false);

        TextView name = (TextView)rowView.findViewById(R.id.name);
        TextView mande = (TextView)rowView.findViewById(R.id.mande);
        name.setText(hesabs.get(position));
        mande.setText("");
        return rowView;
    }
}
